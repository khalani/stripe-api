require('dotenv').config()
const stripe = require("stripe")(process.env.SECRET_KEY);

export const productList = async () => {
    const products = await stripe.products.list()
    const prices = await stripe.prices.list()
    const skus = await stripe.skus.list()
    return Promise.all([products, prices, skus])
        .then(async (response) => {
            const [products, prices, skus] = response
            let productsData = products.data
            let pricesData = prices.data
            let skusData = skus.data
            for (const product of productsData) {
                const sku = skusData.find((sku) => sku.product === product.id)
                if (!sku) {
                    const price = pricesData.find((price) => price.product === product.id)
                    const createdSku = await stripe.skus.create({
                        currency: price.currency,
                        attributes: {
                            size: "",
                            gender: "",
                            color: "",
                        },
                        inventory: { type: "finite", quantity: 500 }, // static for now
                        price: price.unit_amount,
                        product: product.id,
                    })
                    product.price = price.unit_amount
                    product.sku = createdSku.id
                } else {
                    const price = pricesData.find((price) => price.product === product.id)
                    product.price = price.unit_amount
                    product.sku = sku.id
                }
            }
            return productsData
        })
        .catch((e) => {
            throw e
        })
}

export const payment = async ({
    sku,
    email,
    quantity,
    currency,
    shippingDetails,
    cardDetails,
  }) => {
    const order = await stripe.orders.create({
        currency,
        items: [
            {
                type: "sku",
                parent: sku,
                quantity,
            },
        ],
        shipping: shippingDetails,
    });
    const token = await stripe.tokens.create({
        card: cardDetails,
    });
    return await stripe.orders.pay(order.id, {
        source: token.id,
        email,
    });
}