import express from "express";

import { payment, productList } from "../controllers/product.controller";

const router = express.Router();

/* This API gives the available list of product with SKUs and Price */
router.get("/list", async (req, res) => {
  try {
    const products = await productList();
    res.status(200).send(products);
  } catch (e) {
    res.status(400).send(e);
  }
});

/* This API handles the payment part */
router.post("/payment", async (req, res) => {
  const {
    sku,
    email,
    quantity,
    currency,
    shippingDetails,
    cardDetails,
  } = req.body;
  try {
    const paidOrder = await payment({
      sku,
      email,
      quantity,
      currency,
      shippingDetails,
      cardDetails,
    });
    res.status(200).send(paidOrder);
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;
